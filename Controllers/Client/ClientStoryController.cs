﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using e_services.Services;
using e_services.Security;
using e_services.Models;
using e_services.Helper;
using Microsoft.Extensions.Options;
using e_services.Respository;
using e_services.Models.Product;

namespace e_services.Controllers.client
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ClientStoryController : ControllerBase
    {

        private readonly IClientStoryRepository _clientStoryRepository;

        public ClientStoryController(IClientStoryRepository clientStoryService)
        {           
            _clientStoryRepository = clientStoryService;
        }

        //------product-----

        [Authorize(Roles = Role.Client)]
        [HttpPost("followProduct/{id}")]
        public IActionResult postFollowProduct(string id ,[FromForm] Category c ,[FromForm] string idproduct)
        {
            FollowedProduct  follow= _clientStoryRepository.PostFollowedProducts(id,c, idproduct);
            if (User.Identity.Name.Equals(id))
            {
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow });
                }
            }
            return BadRequest(new {status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpGet("followProduct/{id}")]
        public IActionResult getFollowProduct(string id)
        {
            FollowedProduct follow = _clientStoryRepository.getFollowedProducts(id);
            if (User.Identity.Name.Equals(id))
            {
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow });
                }
            }
            return BadRequest(new { status = 404 });
        }


        [Authorize(Roles = Role.Client)]
        [HttpPost("unfollowProduct/{id}")]
        public IActionResult deleteFollowProduct(string id, [FromForm] Category c, [FromForm] string idproduct)
        {
            FollowedProduct follow = _clientStoryRepository.deleteFollowedProducts(id,c,idproduct);
            if (User.Identity.Name.Equals(id))
            {
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow });
                }
            }
            return BadRequest(new { status = 404 });
        }

        //------shop-----

        [Authorize(Roles = Role.Client)]
        [HttpPost("followShop/{id}")]
        public IActionResult postFollowShop(string id,[FromForm]string idShop)
        {        
            if (User.Identity.Name.Equals(id))
            {
                FollowedShop follow = _clientStoryRepository.postFollowedShop(id, idShop);
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow.shop });
                }
            }
            return BadRequest(new { status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpGet("followShop/{id}")]
        public IActionResult getFollowShop(string id)
        {
            FollowedShop follow = _clientStoryRepository.getFollowedShop(id);
            if (User.Identity.Name.Equals(id))
            {
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow.shop }); ;
                }
            }
            return BadRequest(new { status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpPost("unfollowShop/{id}")]
        public IActionResult deleteFollowShop(string id, [FromForm]string idShop)
        {
            FollowedShop follow = _clientStoryRepository.deleteFollowedShop(id, idShop);
            if (User.Identity.Name.Equals(id))
            {
                if (follow != null)
                {
                    return Ok(new { status = 200, payload = follow.shop });
                }
            }
            return BadRequest(new { status = 404 });
        }

        //-------order
        [Authorize(Roles = Role.Client)]
        [HttpPost("order/{id}")]
        public IActionResult postOrder(string id ,[FromForm]Order order )
        {
            if (User.Identity.Name.Equals(id))
            {
                Clientorder orders = _clientStoryRepository.postOrder(id, order);
                if (orders != null)
                {
                    return Ok(new { status = 200 , payload= orders.orders });
                }
            }
            return BadRequest(new { status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpGet("orders/{id}")]
        public IActionResult getOrders(string id)
        {
            if (User.Identity.Name.Equals(id))
            {
                Clientorder orders = _clientStoryRepository.getOrder(id);
                if (orders != null)
                {
                    return Ok(new { status = 200, payload = orders.orders});
                }
            }
            return BadRequest(new { status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpGet("order/{id}")]
        public IActionResult getOrder(string id,string idorder)
        {
            if (User.Identity.Name.Equals(id))
            {
                Order orders = _clientStoryRepository.getOrderById(id,idorder);
                if (orders != null)
                {
                    return Ok(new { status = 200, payload = orders });
                }
            }
            return BadRequest(new { status = 404 });
        }

        [Authorize(Roles = Role.Client)]
        [HttpDelete("order/{id}")]
        public IActionResult deleteOrder(string id ,string idorder)
        {
            if (User.Identity.Name.Equals(id))
            {
                Clientorder orders = _clientStoryRepository.deleteOrder(id, idorder);
                if (orders != null)
                {
                    return Ok(new { status = 200, payload = orders.orders });
                }
            }
            return BadRequest(new { status = 404 , payload="order not found, or you have confirmed it"});
        }
    }
}