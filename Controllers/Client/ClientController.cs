﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using e_services.Services;
using e_services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using e_services.Helper;
using e_services.Respository;
using Microsoft.AspNetCore.Cors;
using e_services.Utility;
using Microsoft.Extensions.Options;
using e_services.Security;
namespace e_services.Controllers.Client
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ClientController : ControllerBase
    {
        //private readonly ClientService _clientService;
        private readonly IEmailSender _emailSender;
        private readonly AppSettings _appSettings;
        private readonly IUserRepository<Models.Client> _clientRepository;
        public static IWebHostEnvironment _environment;

        public ClientController(IEmailSender emailSender, IOptions<AppSettings> appSettings,IUserRepository<Models.Client> clientService,IWebHostEnvironment environment)
        {
            _clientRepository = clientService;
            _environment = environment;
            _emailSender = emailSender;
            _appSettings = appSettings.Value;
        }


        [Authorize(Roles = Role.Client)]
        [HttpGet]
        public IActionResult Get([FromForm] int start, [FromForm]int end) {
            List<Models.Client> clients = _clientRepository.Get(start, end);
            if (clients == null)
            {
                return BadRequest(new { status = 400, payload = new List<Models.Client>() });
            }
            else
            {
                return Ok(new { status = 200, payload = clients });
            }
        }

        /// <summary>
        /// get a specific Client.
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = Role.Client)]
        [HttpGet("{id:length(20)}")]
        public IActionResult Get(string id)
        {
            Models.Client clients = _clientRepository.Get(id);
            if (clients == null)
            {
                return BadRequest(new { status = 400, payload = "no document of id found" });
            }
            else
            {
                return Ok(new { status = 200, payload = clients });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromForm]Models.Client client, [FromForm] string password )
        {
            Models.Client indexedClient = _clientRepository.Create(client,password);

            if(indexedClient==null)
                return BadRequest(new { status = 400, payload = "Account already exist with this email" });

            if (indexedClient.id == null)
            {
                return BadRequest(new { status = 400, payload = "document can not been inserted" });
            }
            else
            {
                return Ok(new { status = 200, payload = indexedClient });

            }
        }

        [Authorize(Roles = Role.Client)]
        [HttpPut("{id:length(20)}")]
        public IActionResult update(string id ,[FromForm]Models.Client client)
        {
            if (User.Identity.Name.Equals(id))
            {
                bool indexedClient = _clientRepository.Update(id, client);
                if (!indexedClient)
                {
                    return BadRequest(new { status = 400, payload = "document can not updated" });
                }
                else
                {
                    return Ok(new { status = 200, payload = client });

                }
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token doesn't refer to id"
                });
            }
            
        }


        [Authorize(Roles = Role.Client)]
        [HttpDelete("{id:length(20)}")]
        public IActionResult delete(string id)
        {
            if (User.Identity.Name.Equals(id))
            {
                bool indexedClient = _clientRepository.Remove(id);
                if (!indexedClient)
                {
                    return BadRequest(new { status = 400, payload = "document can not be deleted" });
                }
                else
                {
                    return Ok(new { status = 200, payload = "deleted" });

                }
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token doesn't refer to id"
                });
            }
            
        }

        [AllowAnonymous]
        [HttpPost("outh")]
        public IActionResult Authenticate([FromForm]string email ,[FromForm] string password)
        {
            Models.Client client  = _clientRepository.getUserByEmail(email);

            if (client == null)
            {
                return BadRequest(new { status=404 , message = "Username or password is incorrect" });

            }
            else
            {
               var token = _clientRepository.Authenticate(client.id, password);

                if (token != null)
                    return Ok(new { status =200 , payload= client, token =token ,role="client"});
                return BadRequest(new { status = 404, message = "Username or password is incorrect" });
            }


        }
        
        //[Authorize(Roles = Role.Client)]
        [Authorize(Roles=Role.Client)]
        [HttpPost("picture/{id}")]
        public async Task<IActionResult> PicturePost([FromForm]FileUploader files, string id)
        {
            string folderName = "\\ClientImages\\";
            if (User.Identity.Name.Equals(id))
            {
                string filename = FileHelper.FileUploader(folderName, files, _environment);
                if (filename != null)
                {
                    Models.Client c = _clientRepository.updatePhoto(id, filename);
                    if (c != null)
                        return Ok(new
                        {
                            status = 200,
                            payload = c
                        });
                }
                return BadRequest(new
                {
                    status = 400,
                    payload = "something went rong"
                });
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token doesn't refer to id"
                });
            }
        }



        [Authorize(Roles = Role.Client)]
        [HttpGet("picture/{id}")]
        public async Task<IActionResult> PictureGet(string id, [FromForm] string url)
        {
            
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot\\clientimages", url);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "image/png", Path.GetFileName(path));
        }


        //email reused controller
        /*[AllowAnonymous]
        [HttpGet("email")]
        public async Task<IActionResult> SendEmail([FromForm]string username, [FromForm]string email)
        {            
            bool status= await _emailSender.SendEmailAsync(email,username);            
            if(status)
                return Ok(new
                {
                    status =200
                });
            return BadRequest(new
            {
                status =400
            });
        }*/

        
        [AllowAnonymous]
        [HttpPost("signinorsignupWithFacebook/{id}")]
        public async Task<IActionResult> socialLogin(string id ,[FromForm] string token)
        {
            bool v = await Utility.Request.facebookVerifyTokenIsLogged(token, id);
            if (v)
            {
                Models.Client c = await Utility.Request.facebookUserValue(token, id);
                if (c != null)
                {
                    string Khomsa_token = _clientRepository.signinorsignupWithFacebook(c);
                    return Ok(new { status = 200, payload = c, token = Khomsa_token , role = "client" });
                }
            }
            return BadRequest(new { status = 404, message = "something went rong" });

        }


        [AllowAnonymous]
        [HttpPost("requestresetpassword")]
        public async Task<IActionResult> SendEmail([FromForm]string email)
        {
            string token = TokenCreator.tokenGenerator(Role.Client, email, _appSettings.Secret);
            bool status = await _emailSender.SendEmailAsync(email, token);
            if (status)
                return Ok(new
                {
                    status = 200
                });
            return BadRequest(new
            {
                status = 400
            });
        }

        [Authorize]
        [HttpPost("resetpassword")]
        public async Task<IActionResult> reset([FromForm]string email, [FromForm]string password)
        {
            if (User.Identity.Name.Equals(email))
            {
                if (_clientRepository.resetPassword(email, password))
                {
                    return Ok(new
                    {
                        status = 200
                    });
                }
                else
                {
                    return BadRequest(new
                    {
                        status = 400
                    });
                }
            }
            else
            {
                return BadRequest(new
                {
                    status = 400
                });
            }


        }
    }
}
