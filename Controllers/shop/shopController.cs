﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using e_services.Respository;
using e_services.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using e_services.Models;
using e_services.Helper;
using Microsoft.AspNetCore.Hosting;

namespace e_services.Controllers.shop
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class shopController : ControllerBase
    {
        //User.Identity.Name
        public static IWebHostEnvironment _environment;
        private readonly IShopRespository _shopRepository;
        public shopController(IShopRespository clientService, IWebHostEnvironment environment)
        {
            _shopRepository = clientService;
            _environment = environment;

        }

        [Authorize(Roles = Role.Hoster)]
        [HttpPost("{id}")]
        public IActionResult create(string id, [FromForm]Shop shop)
        {
            if (User.Identity.Name.Equals(id))
            {
                shop.idHoster = id;
                return Ok(_shopRepository.Create(shop, id));

            }
            return Unauthorized(new 
            {
                status=401,
                payload="the token dosen't refer to id"
            });

        }

        [Authorize(Roles = Role.Hoster + "," + Role.Client)]
        [HttpGet]
        public IActionResult getAll([FromForm]int paginationStart, [FromForm]int paginationEnd)
        {
            List<Shop> myShops = _shopRepository.getAll(paginationStart, paginationEnd);
            if (myShops != null)
            {
                return Ok(new
                {
                    status = 200,
                    payload = myShops
                });
            }

            return BadRequest(new {
                status = 200,
                paylod = "no item found"
            }); ;

        }

        /*[Authorize(Roles = Role.Hoster)]
        [HttpGet("ofhoster")]
        public IActionResult get()
        {//User.Identity.Name
            string id = User.Identity.Name;
            return Ok(id);
            List<Shop> list_of_shops = _shopRepository.getHosterShop(id);
            if (list_of_shops != null)
                return Ok(new
                {
                    status = 200,
                    payload = list_of_shops
                });
            return BadRequest(new
            {
                stauts = 403,
                payload = "new element found for " + id
            });

        }*/

        /// <summary>
        /// get shop by id
        /// </summary>
        [Authorize(Roles = Role.Hoster + "," + Role.Client)]
        [HttpGet("{id}")]
        public IActionResult get(string id)
        {//User.Identity.Name

            Shop shop_detail = _shopRepository.getByIdShop(id);
            if (shop_detail != null)
                return Ok(new
                {
                    status = 200,
                    payload = shop_detail
                });
            return BadRequest(new
            {
                stauts = 403,
                payload = "new element found for " + id
            });

        }

        [Authorize(Roles = Role.Hoster)]
        [HttpDelete("{idHoster}")]
        public IActionResult remove(string idHoster, [FromForm]string idShop)
        {
            if (User.Identity.Name.Equals(idHoster))
            {
                bool action = _shopRepository.Remove(idShop, idHoster);
                if (action)
                    return Ok(new
                    {
                        status = 200,
                        payload = "deleted"
                    });
                return BadRequest(new
                {
                    StatusCode = 400,
                    payload = "something went rong"
                });
            }
            else
            {
                return Unauthorized(new
                {
                    status=401,
                    payload="token doesn't refer to id"
                });
            }
           

        }

        /// <summary>
        /// update shop
        /// idShop is header parameter
        /// </summary>
        [Authorize(Roles = Role.Hoster)]
        [HttpPut("{idHoster}")]
        public IActionResult update ([FromForm]Shop shop , string idHoster )
        {
            if (User.Identity.Name.Equals(idHoster))
            {
                string idShop = Request.Headers["idshop"];
                if (idShop != null)
                {
                    bool isUpdated = _shopRepository.Update(idHoster, idShop, shop);
                    if (isUpdated)
                        return Ok(new
                        {
                            status = 200,
                            payload = "update"
                        });
                    return BadRequest(new
                    {
                        status = 400,
                        payload = "something went rong"
                    });
                }
                return BadRequest(new
                {
                    error = "idShop filed is required"
                });
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload="token doesn't refer to id"
                });
            }
            
        }

        /// <summary>
        /// update or post shop photo
        /// idhoster and idshop is a headers paramters
        /// </summary>
        [Authorize(Roles = Role.Hoster)]
        [HttpPost("photo")]
        public IActionResult postPhoto([FromForm]FileUploader files)
        {
            string idshop = Request.Headers["idshop"];
            string idHoster = Request.Headers["idhoster"];

            if((idHoster!=null)&&(idshop!=null))
                if (User.Identity.Name.Equals(idHoster))
                {
                    string filename = FileHelper.FileUploader("\\shopMedia\\", files, _environment);
                    if (filename != null)
                    {
                        Shop shop = _shopRepository.updatePhoto(idshop, filename);
                        if (shop != null)
                            return Ok(new
                            {
                                status = 200,
                                payload = shop
                            });

                        return BadRequest(new
                        {
                            status = 400,
                            payload = "something went rong with"
                        });

                    }
                }
                else
                {
                    return Unauthorized(new
                    {
                        status = 401,
                        payload = "token doesn't refer to id"
                    });
                }

            return BadRequest(new
            {
                status = 400,
                payload = "idHoster or idShop fields is not found"
            });             
        }

        /// <summary>
        /// post or update  shop video
        /// idhoster and idshop is a headers paramters
        /// </summary>
        [Authorize(Roles = Role.Hoster)]
        [HttpPost("video")]
        public IActionResult postVideo([FromForm]FileUploader files)
        {
            string idshop = Request.Headers["idshop"];
            string idHoster = Request.Headers["idhoster"];

            if ((idHoster != null) && (idshop != null))
                if (User.Identity.Name.Equals(idHoster))
                {
                    string filename = FileHelper.FileUploader("\\shopMedia\\", files, _environment);
                    if (filename != null)
                    {
                        Shop shop = _shopRepository.updateVideo(idshop, filename);
                        if (shop != null)
                            return Ok(new
                            {
                                status = 200,
                                payload = shop
                            });

                        return BadRequest(new
                        {
                            status = 400,
                            payload = "something went rong with"
                        });

                    }
                }
                else
                {
                    return Unauthorized(new
                    {
                        status = 401,
                        payload = "token doesn't refer to id"
                    });
                }

            return BadRequest(new
            {
                status = 400,
                payload = "idHoster or idShop fields is not found"
            });
        }



    }
}