﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using e_services.Helper;
using e_services.Models;
using e_services.Respository;
using e_services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IHoster = e_services.Models.Hoster.Hoster;

namespace e_services.Controllers.Hoster
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class HosterController : ControllerBase
    {
        private readonly IUserRepository<IHoster> _hosterRepository;
        public static IWebHostEnvironment _environment;

        public HosterController(IUserRepository<IHoster> clientService, IWebHostEnvironment environment)
        {
            _hosterRepository = clientService;
            _environment = environment;
        }

        [Authorize(Roles = Role.Hoster)]
        [HttpGet]
        public IActionResult Get([FromForm] int start, [FromForm]int end)
        {
            List<IHoster> hosters = _hosterRepository.Get(start, end);
            if (hosters == null)
            {
                return BadRequest(new { status = 400, payload = new List<IHoster>() });
            }
            else
            {
                return Ok(new { status = 200, payload = hosters });
            }
        }

        /// <summary>
        /// get a specific Client.
        /// </summary>
        /// <param name="id"></param>
        [Authorize(Roles = Role.Hoster)]
        [HttpGet("{id:length(20)}")]
        public IActionResult Get(string id)
        {
            IHoster hoster = _hosterRepository.Get(id);
            if (hoster == null)
            {
                return BadRequest(new { status = 400, payload = "no document of id found" });
            }
            else
            {
                return Ok(new { status = 200, payload = hoster });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromForm]IHoster hoster, [FromForm] string password)
        {
            IHoster indexedHoster = _hosterRepository.Create(hoster, password);

            if (indexedHoster == null)
                return BadRequest(new { status = 400, payload = "Account already exist with this email" });

            if (indexedHoster.id == null)
            {
                return BadRequest(new { status = 400, payload = "document can not been inserted" });
            }
            else
            {
                return Ok(new { status = 200, payload = indexedHoster });

            }
        }

        [Authorize(Roles = Role.Hoster)]
        [HttpPut("{id:length(20)}")]
        public IActionResult update(string id, [FromForm]IHoster hoster)
        {
            if (User.Identity.Name.Equals(id))
            {
                bool indexedHoster = _hosterRepository.Update(id, hoster);
                if (!indexedHoster)
                {
                    return BadRequest(new { status = 400, payload = "document can not updated" });
                }
                else
                {
                    return Ok(new { status = 200, payload = hoster });

                }
            }
            else
            {
                return Unauthorized(new
                {
                    status =401,
                    payload ="token doesn't refer to id"
                });
            }           
        }


        [Authorize(Roles = Role.Hoster)]
        [HttpDelete("{id:length(20)}")]
        public IActionResult delete(string id)
        {
            if (User.Identity.Name.Equals(id))
            {
                bool indexedHoster = _hosterRepository.Remove(id);
                if (!indexedHoster)
                {
                    return BadRequest(new { status = 400, payload = "document can not be deleted" });
                }
                else
                {
                    return Ok(new { status = 200, payload = "deleted" });

                }
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token doesn't refer to id"
                });
            }
            
        }

        [AllowAnonymous]
        [HttpPost("outh")]
        public IActionResult Authenticate([FromForm]string email, [FromForm] string password)
        {
            IHoster hoster = _hosterRepository.getUserByEmail(email);

            if (hoster == null)
            {
                return BadRequest(new { status = 404, message = "Username or password is incorrect" });

            }
            else
            {
                var token = _hosterRepository.Authenticate(hoster.id, password);

                if (token != null)
                    return Ok(new { status = 200, payload = hoster, token = token, role = "hoster" });
                return BadRequest(new { status = 404, message = "Username or password is incorrect" });
            }


        }

        //[Authorize(Roles = Role.Client)]
        [Authorize(Roles = Role.Hoster)]
        [HttpPost("picture/{id}")]
        public async Task<IActionResult> PicturePost([FromForm]FileUploader files, string id)
        {
            string folderName = "\\HosterImages\\";
            if (User.Identity.Name.Equals(id))
            {
                string filename = FileHelper.FileUploader(folderName, files, _environment);
                if (filename != null)
                {
                    IHoster h = _hosterRepository.updatePhoto(id, filename);
                    if (h != null)
                        return Ok(new
                        {
                            status = 200,
                            payload = h
                        });                   
                }
                return BadRequest(new
                {
                    status = 400,
                    payload = "something went rong"
                });
            }
            else
            {
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token doesn't refer to id"
                });
            }
        }


        [Authorize(Roles = Role.Hoster)]
        [HttpGet("picture/{id}")]
        public async Task<IActionResult> PictureGet(string id, [FromForm] string url)
        {

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot\\HosterImages", url);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, "image/png", Path.GetFileName(path));
        }

        
    }
}