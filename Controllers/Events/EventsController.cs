﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using e_services.Respository;
using e_services.Models;

namespace e_services.Controllers.Events
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventRepository _evnetRepository;

        public EventsController(IEventRepository clientService)
        {
            _evnetRepository = clientService;
        }

        [Authorize(Roles = Role.Client+","+Role.Hoster)]
        [HttpPost]
        public IActionResult getall([FromForm]int from , [FromForm]int size)
        {
            List<Event> myEvents = _evnetRepository.getAllEvent(from, size);
            if (myEvents != null)
                return Ok(myEvents);
            return BadRequest(); 
        } 
    }
}