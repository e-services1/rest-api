﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models;
using e_services.Models.Product;
using e_services.Respository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace e_services.Controllers.product
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ProductStoryController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        public ProductStoryController(IProductRepository productService)
        {
            _productRepository = productService;

        }

        [Authorize(Roles = Role.Hoster + "," + Role.Client)]
        [HttpPost("searchbyname")]
        public IActionResult searchByName([FromForm]string name,[FromForm] int from, [FromForm]int size)
        {
            List<Product> p = _productRepository.searchByName(name,from,size);
            if (p == null)
                return BadRequest(new { payload= "no element found"});
            return Ok(new
            {
                payload=p 
            });
        }

        [AllowAnonymous]
        [HttpPost("searchbycategory")]
        public IActionResult searchByCategory([FromForm]Category category, [FromForm]string name, [FromForm] int from, [FromForm]int size)
        {
            List<Product> p = _productRepository.searchByCategory(category,name,from,size);
            return Ok(new
            {
                payload = p
            });
            if (p == null)
                return BadRequest(new { payload = "no element found" });
            return Ok(new
            {
                payload = p
            });
        }
    }
    
    
}