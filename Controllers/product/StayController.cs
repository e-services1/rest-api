﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Respository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using e_services.Models;
using e_services.Models.Product;
using e_services.Helper;

namespace e_services.Controllers.product
{
    [Authorize]
    [Route("product/[controller]")]
    [ApiController]
    public class StayController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        private readonly IProductRepository _productRepository;
        public StayController(IProductRepository productService, IWebHostEnvironment environment)
        {
            _productRepository = productService;
            _environment = environment;

        }

        [Authorize(Roles = Role.Hoster)]
        [HttpPost]
        public IActionResult create(pStay product)
        {
            if ((product.idHoster!=null) && (product.idShop!=null)) {
                if (User.Identity.Name.Equals(product.idHoster))
                {
                    var b = _productRepository.Create(product, product.idHoster, product.idShop);
                    return Ok(b);
                }
                return Unauthorized(new
                {
                    status = 401,
                    payload = "token does't refer to id"
                });
            }
            return BadRequest(new
            {
                status = 404,
                payload = "headers is required, idshop and idhoster"
            });
        }

        [Authorize(Roles = Role.Hoster + "," + Role.Client)]
        [HttpGet("{id}")]
        public IActionResult get(string id)
        {
            pStay p = _productRepository.Get(id, Category.stay);
            if (p != null)
            {
                return Ok(p);
            }
            return BadRequest(new { status = 404 });

        }

        [Authorize(Roles = Role.Hoster + "," + Role.Client)]
        [HttpGet]
        public IActionResult get(int from , int size)
        {
            List<pStay> p = _productRepository.get(Category.stay,from,size);
            if (p != null)
            {
                return Ok(p);
            }
            return BadRequest(new { status = 404 });

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="product">
        /// idhoster and idshop and id, must be included in product .
        /// </param>
        /// <returns></returns>
        [Authorize(Roles = Role.Hoster)]
        [HttpPut]
        public IActionResult update (pStay product)
        {
            if (User.Identity.Name.Equals(product.idHoster))
            {
                if ((product.idHoster!=null) && (product.idShop!=null)&&(product.id!=null))
                {
                    bool isUpdated = _productRepository.Update(product);
                    if (isUpdated)
                        return Ok(new {payload= product });
                    return BadRequest();

                }
                return BadRequest();
            }

            return Unauthorized();
        }

        [Authorize(Roles = Role.Hoster)]
        [HttpDelete]
        public IActionResult remove(pStay product)
        {
            if (User.Identity.Name.Equals(product.idHoster))
            {
                if ((product.idHoster != null) && (product.idShop != null) && (product.id != null))
                {
                    bool isDleted = _productRepository.Remove(product);
                    if (isDleted)
                        return Ok(new { payload = "deleted" });
                    return BadRequest(new { payload = "something went rong"});

                }
                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// update or post product photo
        /// idhoster and idproduct is a headers paramters
        /// </summary>
        [Authorize(Roles = Role.Hoster)]
        [HttpPost("photo")]
        public IActionResult postPhoto([FromForm]FileUploader files)
        {
            string idProduct = Request.Headers["idproduct"];
            string idHoster = Request.Headers["idhoster"];

            if ((idHoster != null) && (idProduct != null))
                if (User.Identity.Name.Equals(idHoster))
                {
                    string filename = FileHelper.FileUploader("\\stayMedia\\", files, _environment);
                    if (filename != null)
                    {
                        pStay stay = _productRepository.updatePhoto(idProduct, filename,Category.stay);
                        if (stay != null)
                            return Ok(new
                            {
                                status = 200,
                                payload = stay
                            });

                        return BadRequest(new
                        {
                            status = 400,
                            payload = "something went rong with"
                        });

                    }
                }
                else
                {
                    return Unauthorized(new
                    {
                        status = 401,
                        payload = "token doesn't refer to id"
                    });
                }

            return BadRequest(new
            {
                status = 400,
                payload = "idHoster or idShop fields is not found"
            });
        }

    }
}