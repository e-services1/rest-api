﻿using e_services.Helper;
using e_services.Models;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Respository;

namespace e_services.Services
{
    public class EventService : IEventRepository
    {
        private readonly ElasticClient _client;
        private readonly AppSettings _appSettings;
        private readonly string eventIndex = "event";

        public EventService(IElasticSearchSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            var node = new Uri(settings.uri);

            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(eventIndex);

            _client = new ElasticClient(settingsNode);

        }

        public List<Event> getAllEvent(int paginationStart, int paginationEnd)
        {
            var indexResponse = _client.Search<Event>(i => i.Index(eventIndex)
            .From(paginationStart)
            .Size(paginationEnd)
            .Query(q => q.MatchAll()));

            if ((indexResponse.IsValid)&&(indexResponse.Hits.Count>0))
            {
                List<Event> events = new List<Event>();
                events= indexResponse.Hits.Select(item =>
                 {
                     item.Source.id = item.Id;
                     return item.Source;
                 }).ToList();
                return events;
            }
            return null;
        }
    }
}
