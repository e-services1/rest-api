﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Respository;
using e_services.Models;
using Nest;
using e_services.Helper;
using Microsoft.Extensions.Options;
using e_services.Models.Product;
using e_services.Utility;
namespace e_services.Services
{
    public class ClientStoryService : IClientStoryRepository
    {
        private readonly ElasticClient _client;
        private readonly string followProductIndex = ElasticIndex.followProductIndex;
        private readonly string followShopIndex = ElasticIndex.followShopIndex;
        private readonly string orderIndex = ElasticIndex.orderIndex;
        private readonly string clientOrderIndex = ElasticIndex.clientOrderIndex;
        private readonly string stayIndex = ElasticIndex.stayIndex;
        private readonly string adventureIndex = ElasticIndex.adventureIndex;
        private readonly string productIndex = ElasticIndex.productIndex;

        public ClientStoryService(IElasticSearchSettings settings)
        {
            var node = new Uri(settings.uri);

            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(followProductIndex);

            _client = new ElasticClient(settingsNode);

        }
        
        //-----follow product
        public FollowedProduct deleteFollowedProducts(string iduser,Category c ,string idproduct)
        {
            FollowedProduct follow;
            Product? p;
            var indexResponse = _client.Get<FollowedProduct>(iduser, id => id.Index(followProductIndex));
            if (indexResponse.IsValid)
            {
                follow = indexResponse.Source;
                if (c.Equals(Category.stay))
                {
                    p = _client.Get<Product>(idproduct, u => u.Index(stayIndex)).Source;
                    p.like--;

                    if (follow.stay != null)
                    {
                        follow.stay.Remove(idproduct);
                        _client.Update<FollowedProduct>(iduser, u => u.Index(followProductIndex).Doc(follow));
                        _client.Update<Product>(idproduct, u => u.Index(stayIndex).Doc(p));
                        return follow;
                    }
                    return null;
                }
                if (c.Equals(Category.adventure))
                {
                    p = _client.Get<Product>(idproduct, u => u.Index(adventureIndex)).Source;
                    p.like--;
                    if (follow.adventure != null)
                    {
                        follow.adventure.Remove(idproduct);
                        _client.Update<FollowedProduct>(iduser, u => u.Index(followProductIndex).Doc(follow));
                        _client.Update<Product>(idproduct, u => u.Index(adventureIndex).Doc(p));
                        return follow;
                    }
                    return null;
                }
                if (c.Equals(Category.product))
                {
                    p = _client.Get<Product>(idproduct, u => u.Index(productIndex)).Source;
                    p.like--;
                    if (follow.product != null)
                    {
                        follow.product.Remove(idproduct);
                        _client.Update<FollowedProduct>(iduser, u => u.Index(followProductIndex).Doc(follow));
                        _client.Update<Product>(idproduct, u => u.Index(productIndex).Doc(p));
                        return follow;
                    }
                    return null;
                }
            }
            return null;
        }
        public FollowedProduct getFollowedProducts(string iduser)
        {
            var indexResponse = _client.Get<FollowedProduct>(iduser);
            if (indexResponse.IsValid)
                if (indexResponse.Source != null)
                    return  indexResponse.Source;
            return null;
        }               
        public FollowedProduct PostFollowedProducts(string iduser, Category c, string idproduct)
        {
            FollowedProduct? follow;
            Product? p;
            follow = _client.Get<FollowedProduct>(iduser, id =>id.Index(followProductIndex))?.Source;
            if (follow == null)
                follow = new FollowedProduct();
            if (c.Equals(Category.stay))
            {
                p = _client.Get<Product>(idproduct, u => u.Index(stayIndex))?.Source;
                if (p == null)
                    return null;
                p.like++;
                if (follow.stay != null)
                {
                    if(follow.stay.IndexOf(idproduct)==-1)
                    {
                        _client.Update<Product>(idproduct, u => u.Doc(p).Index(stayIndex));
                        follow.stay.Add(idproduct);
                    }
                }
                else
                {
                    follow.stay = new List<string>();
                    follow.stay.Add(idproduct);
                    _client.Update<Product>(idproduct, u => u.Doc(p).Index(stayIndex));

                }
            }
            if (c.Equals(Category.adventure))
            {
                p = _client.Get<Product>(idproduct, u => u.Index(adventureIndex))?.Source;
                if (p == null)
                    return null;
                p.like++;
                if (follow.adventure != null)
                {
                    if (follow.adventure.IndexOf(idproduct) == -1)
                    {
                        _client.Update<Product>(idproduct, u => u.Doc(p).Index(adventureIndex));
                        follow.adventure.Add(idproduct);
                    }
                }
                else
                {
                    follow.adventure = new List<string>();
                    follow.adventure.Add(idproduct);
                    _client.Update<Product>(idproduct, u => u.Doc(p).Index(adventureIndex));

                }
            }
            if (c.Equals(Category.product))
            {
                p = _client.Get<Product>(idproduct, u => u.Index(productIndex))?.Source;
                if (p == null)
                    return null;
                p.like++;
                if(follow.product != null)
                {
                    if (follow.product.IndexOf(idproduct) == -1)
                    {
                        _client.Update<Product>(idproduct, u => u.Doc(p).Index(productIndex));                   
                        follow.product.Add(idproduct);
                    }
                }
                else
                {
                    follow.product = new List<string>();
                    follow.product.Add(idproduct);
                    _client.Update<Product>(idproduct, u => u.Doc(p).Index(productIndex));
                }
            }
            
            var rsp = _client.Index(follow ,i=>i.Index(followProductIndex).Id(iduser));
            if (rsp.IsValid)
                return follow;
            return null;


        }
        //---- follow shop
        public FollowedShop postFollowedShop(string iduser, string shop)
        {
            FollowedShop l;
            var indexResponse = _client.Get<FollowedShop>(iduser, u => u.Index(followShopIndex));
            if ((indexResponse.IsValid)&&(indexResponse.Source!=null))
            {
                l = indexResponse.Source;
                if (l.shop.IndexOf(shop) == -1)
                {
                    l.shop.Add(shop);
                    _client.Update<FollowedShop>(iduser, u => u.Doc(l).Index(followShopIndex));
                }
                return l;
            }
            //add for the first time !
            l = new FollowedShop();
            l.shop = new List<string>();
            if (l.shop.IndexOf(shop)==-1)
            {
                l.shop.Add(shop);
                var insertResponse = _client.Index(l, u => u.Index(followShopIndex).Id(iduser));
            }
            return l;
        }
        public FollowedShop getFollowedShop(string iduser)
        {

            var indexResponse = _client.Get<FollowedShop>(iduser, u => u.Index(followShopIndex));
            if (indexResponse.Source != null)
                return indexResponse.Source;
            return null;
        }
        public FollowedShop deleteFollowedShop(string iduser, string idshop)
        {
            var indexResponse = _client.Get<FollowedShop>(iduser, u => u.Index(followShopIndex));
            if (indexResponse.Source != null)
            {
                FollowedShop l = indexResponse.Source;
                l.shop.Remove(idshop);
                var deletedResponse = _client.Update<FollowedShop>(iduser, u => u.Index(followShopIndex).Doc(l));
                return l;
            }
            return null;
        }
        //---- order
        public Clientorder postOrder(string id, Order order)
        {
            if (order.idProduct == null)
                return null;
            order.verifyed = false;
            order.idClient = id;
            var indexResponse = _client.Index(order, u => u.Index(orderIndex));
            if (indexResponse.Id != null)
            {
                string cmd_id = indexResponse.Id;
                Clientorder cmds;
                var client_cmds = _client.Get<Clientorder>(id, u => u.Index(clientOrderIndex));
                if (client_cmds.Source != null)
                {
                    if(client_cmds.Source.orders != null)
                    {
                        cmds = client_cmds.Source;
                        cmds.orders.Add(cmd_id);
                        var isOrderUpdated = _client.Update<Clientorder>(id, u => u.Doc(cmds).Index(clientOrderIndex));
                        return cmds;
                    }
                }
                cmds = new Clientorder();
                cmds.orders = new List<string>();
                cmds.orders.Add(cmd_id);
                var isOrder = _client.Index(cmds, u => u.Index(clientOrderIndex).Id(id));
                return cmds;
            }
            return null;
        }
        public Clientorder getOrder(string id)
        {
            var indexResponse = _client.Get<Clientorder>(id,u=>u.Index(clientOrderIndex));
            if (indexResponse.Source != null)
                return indexResponse.Source;
            return null;
        }
        public Clientorder deleteOrder(string id, string idorder)
        {
            var indexResponse = _client.Get<Clientorder>(id, u => u.Index(clientOrderIndex));
            if ((indexResponse.Source != null)&&
                (indexResponse.Source.orders!=null)&&
                (indexResponse.Source.orders.IndexOf(idorder)>=0))
            {
                var isOrderVerifyed = _client.Get<Order>(idorder, u => u.Index(orderIndex));
                if ((isOrderVerifyed.Source!= null)&&(!isOrderVerifyed.Source.verifyed))
                {
                    Clientorder c = indexResponse.Source;
                    c.orders.Remove(idorder);
                    _client.Update<Clientorder>(id, u => u.Doc(c).Index(clientOrderIndex));
                    _client.Delete<Order>(idorder, u => u.Index(orderIndex));
                    return c;
                }             
            }
            return null;
        }
        public Order getOrderById(string idclient, string idOrder)
        {
            var indexResponse = _client.Get<Clientorder>(idclient,u=>u.Index(clientOrderIndex));
            if ((indexResponse.Source != null)&&(indexResponse.Source.orders != null)){
                if (indexResponse.Source.orders.IndexOf(idOrder) >= 0)
                {
                    var orderResponse = _client.Get<Order>(idOrder, u => u.Index(orderIndex));
                    if (orderResponse.Source != null)
                    {
                        orderResponse.Source.id = orderResponse.Id;
                        return orderResponse.Source;

                    }
                }
            }
            return null;
        }
        //------
    }
}
