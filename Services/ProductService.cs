﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Respository;
using e_services.Models.Product;
using Nest;
using e_services.Helper;
using e_services.Models;
using e_services.Models.Hoster;
using Microsoft.Extensions.Options;

namespace e_services.Services
{
    public class ProductService : IProductRepository
    {
        private readonly ElasticClient _product;
        private readonly AppSettings _appSettings;
        private readonly string shopIndex = "shop";
        private readonly string hosterIndex = "hoster";
        private readonly string stayIndex = "pstay";
        private readonly string adventureIndex = "padventure"; 
        private readonly string productIndex = "pproduct";

        /// <summary>
        /// change index each time you operate 
        /// </summary>
        /// <param name="settings">elasticsearch client nest settings</param>
        /// <param name="appSettings"></param>

        public ProductService(IElasticSearchSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            var node = new Uri(settings.uri);

            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(shopIndex);

            _product = new ElasticClient(settingsNode);

        }

        
        public dynamic Create(dynamic product, string idHoster, string idShop)
        {
            if(product is pStay)
            {
                pStay myproduct = (pStay)product;
                myproduct.idHoster = idHoster;
                myproduct.idShop = idShop;
                var productResponse = _product.Index<pStay>(myproduct, id =>id.Index(stayIndex));
                myproduct.id = productResponse.Id;
                var shopResponse = _product.Get<Shop>(idShop);
                if (shopResponse.Source != null)
                {
                    Shop myShop = shopResponse.Source;
                    if (myShop.pStay != null)
                    {
                        myShop.pStay.Add(productResponse.Id);
                         _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        

                    }
                    else
                    {
                        myShop.pStay = new List<string>();
                        myShop.pStay.Add(productResponse.Id);
                        _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        
                    }
                        
                    return myproduct;
                }
                else
                {
                    return null;
                }

            }
            if (product is pAdventure)
            {
                pAdventure myproduct = (pAdventure)product;
                myproduct.idHoster = idHoster;
                myproduct.idShop = idShop;
                var productResponse = _product.Index<pAdventure>(myproduct, id => id.Index(adventureIndex));
                myproduct.id = productResponse.Id;
                var shopResponse = _product.Get<Shop>(idShop);
                if (shopResponse.Source != null)
                {
                    Shop myShop = shopResponse.Source;
                    if (myShop.pAdventure != null)
                    {
                        myShop.pAdventure.Add(productResponse.Id);
                        _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        
                    }
                    else
                    {
                        myShop.pAdventure = new List<string>();
                        myShop.pAdventure.Add(productResponse.Id);
                        _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        
                    }

                    return myproduct;
                }
                else
                {
                    return null;
                }

            }
            if (product is pProduct)
            {
                pProduct myproduct = (pProduct)product;
                myproduct.idHoster = idHoster;
                myproduct.idShop = idShop;
                var productResponse = _product.Index<pProduct>(myproduct, id => id.Index(productIndex));
                myproduct.id = productResponse.Id;
                var shopResponse = _product.Get<Shop>(idShop);
                if (shopResponse.Source != null)
                {
                    Shop myShop = shopResponse.Source;
                    if (myShop.pProduct != null)
                    {
                        myShop.pProduct.Add(productResponse.Id);
                        _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        
                    }
                    else
                    {
                        myShop.pProduct = new List<string>();
                        myShop.pProduct.Add(productResponse.Id);
                        _product.Update<Shop>(idShop, id => id.Doc(myShop).Index(shopIndex));
                        
                    }
                    return myproduct;

                }
                else
                {
                    return null;
                }

            }
            
            return null;
        }

        public dynamic Get(string idPrduct, Category c)
        {
            if (c.Equals(Category.stay))
            {
                var indexResponse = _product.Get<pStay>(idPrduct, idx=>idx .Index(stayIndex));
                if (indexResponse.Source != null)
                {
                    indexResponse.Source.id = indexResponse.Id;
                    return indexResponse.Source; 
                }
                return null;

            }
            if (c.Equals(Category.adventure))
            {
                var indexResponse = _product.Get<pAdventure>(idPrduct, idx => idx.Index(adventureIndex));
                if (indexResponse.Source != null)
                {
                    indexResponse.Source.id = indexResponse.Id;
                    return indexResponse.Source;
                }
                return null;

            }
            if (c.Equals(Category.product))
            {
                var indexResponse = _product.Get<pProduct>(idPrduct, idx => idx.Index(productIndex));
                if (indexResponse.Source != null)
                {
                    indexResponse.Source.id = indexResponse.Id;
                    return indexResponse.Source;
                }
                return null;

            }
            return null;
        }

        public dynamic get(Category c,int from,int size)
        {
            if (c.Equals(Category.stay))
            {
                var indexResponse = _product.Search<pStay>(s => s
            .From(from)
            .Size(size)
            .Index(stayIndex)
            .Query(q => q.MatchAll()));
                if (indexResponse.IsValid)
                {
                    return indexResponse.Hits.Select(h =>
                    {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                }
                return null;
            }
            if (c.Equals(Category.adventure))
            {
                var indexResponse = _product.Search<pAdventure>(s => s
            .From(from)
            .Size(size)
            .Index(adventureIndex)
            .Query(q => q.MatchAll()));
                if (indexResponse.IsValid)
                {
                    return indexResponse.Hits.Select(h =>
                    {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                }
                return null;
            }
            if (c.Equals(Category.product))
            {
                var indexResponse = _product.Search<pProduct>(s => s
            .From(from)
            .Size(size)
            .Index(productIndex)
            .Query(q => q.MatchAll()));
                if (indexResponse.IsValid)
                {
                    return indexResponse.Hits.Select(h =>
                    {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                }
                return null;
            }
            return null;
        }

        public bool Remove(Product product)
        {
            if (product.category.Equals(Category.stay))
            {
                var indexResponse = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                if (indexResponse.Source != null)
                {
                    Shop newShop = indexResponse.Source;
                    if (!newShop.pStay.Contains(product.id))
                        return false;
                    newShop.pStay.Remove(product.id);
                    _product.Update<Shop>(product.idShop, id => id.Doc(newShop).Index(shopIndex));
                    var isdeleted = _product.Delete<pStay>(product.id, id => id.Index(stayIndex));
                    return isdeleted.IsValid;
                }
                return false;
            }
            if (product.category.Equals(Category.adventure))
            {
                var indexResponse = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                if (indexResponse.Source != null)
                {
                    Shop newShop = indexResponse.Source;
                    if (!newShop.pAdventure.Contains(product.id))
                        return false;
                    newShop.pAdventure.Remove(product.id);
                    _product.Update<Shop>(product.idShop, id => id.Doc(newShop).Index(shopIndex));
                    var isdeleted = _product.Delete<pAdventure>(product.id, id => id.Index(adventureIndex));
                    return isdeleted.IsValid;
                }
                return false;
            }
            if (product.category.Equals(Category.product))
            {
                var indexResponse = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                if (indexResponse.Source != null)
                {
                    Shop newShop = indexResponse.Source;
                    if (!newShop.pProduct.Contains(product.id))
                        return false;
                    newShop.pProduct.Remove(product.id);
                    _product.Update<Shop>(product.idShop, id => id.Doc(newShop).Index(shopIndex));
                    var isdeleted = _product.Delete<pProduct>(product.id, id => id.Index(productIndex));
                    return isdeleted.IsValid;
                }
                return false;
            }
            return false;
        }
                
        public bool Update(Product product)
        {
            var is_shop_of_hoster = _product.Get<Hshop>(product.idHoster, id => id.Index(hosterIndex));
            if (is_shop_of_hoster.Source != null)
            {
                if (is_shop_of_hoster.Source.shop.Contains(product.idShop))
                {
                    if(product.category.Equals(Category.stay))
                    {
                        pStay myProduct = (pStay)product;
                        var is_prod_of_shop = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                        if (is_prod_of_shop.Source != null)
                        {
                            if (is_prod_of_shop.Source.pStay.Contains(product.id))
                            {
                                return _product.Update<pStay>(product.id, id => id.Doc(myProduct).Index(stayIndex)).IsValid;
                            }
                            return false;
                        }
                        return false;
                    }
                    if (product.category.Equals(Category.adventure))
                    {
                        pAdventure myProduct = (pAdventure)product;
                        var is_prod_of_shop = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                        if (is_prod_of_shop.Source != null)
                        {
                            if (is_prod_of_shop.Source.pAdventure.Contains(product.id))
                            {
                                var res = _product.Update<pAdventure>(product.id, id => id.Doc(myProduct).Index(adventureIndex));
                                return res.IsValid;
                            }
                            return false;
                        }
                        return false;
                    }
                    if (product.category.Equals(Category.product))
                    {
                        pProduct myProduct = (pProduct)product;
                        var is_prod_of_shop = _product.Get<Shop>(product.idShop, id => id.Index(shopIndex));
                        if (is_prod_of_shop.Source != null)
                        {
                            if (is_prod_of_shop.Source.pProduct.Contains(product.id))
                            {
                                var res = _product.Update<pProduct>(product.id, id => id.Doc(myProduct).Index(productIndex));
                                return res.IsValid;
                            }
                            return false;
                        }
                        return false;
                    }
                }
                return false;
            }
            return false;
        }

        public dynamic updatePhoto(string idproduct, string filename ,Category c)
        {
            if (c.Equals(Category.stay))
            {
                var indexRespons = _product.Get<pStay>(idproduct, id => id.Index(stayIndex));
                if (indexRespons.Source != null)
                {
                    pStay p = indexRespons.Source;
                    if (p.urlPhotos == null)
                    {
                        p.urlPhotos = new List<string>();
                        p.urlPhotos.Add(filename);
                    }
                    else
                    {
                        p.urlPhotos.Add(filename);
                    }
                    _product.Update<pStay>(idproduct, id => id.Doc(p).Index(stayIndex));
                    return p;
                }
            }
            if (c.Equals(Category.adventure))
            {
                var indexRespons = _product.Get<pAdventure>(idproduct, id => id.Index(adventureIndex));
                if (indexRespons.Source != null)
                {
                    pAdventure p = indexRespons.Source;
                    if (p.urlPhotos == null)
                    {
                        p.urlPhotos = new List<string>();
                        p.urlPhotos.Add(filename);
                    }
                    else
                    {
                        p.urlPhotos.Add(filename);
                    }
                    _product.Update<pAdventure>(idproduct, id => id.Doc(p).Index(adventureIndex));
                    return p;
                }
            }
            if (c.Equals(Category.product))
            {
                var indexRespons = _product.Get<pProduct>(idproduct, id => id.Index(productIndex));
                if (indexRespons.Source != null)
                {
                    pProduct p = indexRespons.Source;
                    if (p.urlPhotos == null)
                    {
                        p.urlPhotos = new List<string>();
                        p.urlPhotos.Add(filename);
                    }
                    else
                    {
                        p.urlPhotos.Add(filename);
                    }
                    _product.Update<pProduct>(idproduct, id => id.Doc(p).Index(productIndex));
                    return p;
                }
            }
            return null;            
        }

        public List<Product> searchByCategory(Category c ,string search , int from, int size)
        {
            if (string.IsNullOrEmpty(search))
                return null;

            if (c.Equals(Category.stay))
            {
                var searchResponse = _product.Search<Product>(s => s
                .Index(stayIndex)
                .From(from)
                .Size(size)
                .Query(q => q
                    .MatchPhrasePrefix(m => m
                        .Field(f => f.title)
                        .Field(f=>f.description)
                        .Query(search)
                        )));
                if ((searchResponse.IsValid)&&(searchResponse.Hits.Count > 0))
                {
                    List<Product> p = searchResponse.Hits.Select(h => {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                    return p;
                }
                return null;
            }
            if (c.Equals(Category.adventure))
            {
                var searchResponse = _product.Search<Product>(s => s
                .Index(adventureIndex)
                .Query(q => q
                    .MatchPhrasePrefix(m => m
                        .Field(f => f.title)
                        .Field(f => f.description)
                        .Query(search)
                        )));
                if ((searchResponse.IsValid) && (searchResponse.Hits.Count > 0))
                {
                    List<Product> p = searchResponse.Hits.Select(h => {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                    return p;
                }
                return null;
            }
            if (c.Equals(Category.product))
            {
                var searchResponse = _product.Search<Product>(s => s
                .Index(productIndex)
                .Query(q => q
                    .MatchPhrasePrefix(m => m
                        .Field(f => f.title)
                        .Field(f => f.description)
                        .Query(search)
                        )));
                if ((searchResponse.IsValid) && (searchResponse.Hits.Count > 0))
                {
                    List<Product> p = searchResponse.Hits.Select(h => {
                        h.Source.id = h.Id;
                        return h.Source;
                    }).ToList();
                    return p;
                }
                return null;
            }
            return null;  
        }

        public List<Product> searchByName(string search , int from , int size)
        {
            if (string.IsNullOrEmpty(search))
                return null;

            var searchResponse = _product.Search<Product>(s => s
                .Index("pstay,padventure,pproduct")
                .From(from)
                .Size(size)
                .Query(q => q
                    .MatchPhrasePrefix(m => m
                        .Field(f => f.title)
                        .Field(f => f.description)
                        .Query(search)
                        )));
            /*.From(from)
            .Size(size)
                .Query(q => q
                    .MultiMatch(m => m
                        .Fields(f => f
                            .Field(f => f.title)
                            .Field(f => f.description))
                        .Query(search))));*/

            if ((searchResponse.IsValid)&&(searchResponse.Hits.Count>0))
            {
                List<Product> p = searchResponse.Hits.Select(h => {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList();
                return p;
            }
            return null;
        }

    }
}
