﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Respository;
using e_services.Models;
using e_services.Models.Hoster;
using Nest;
using e_services.Helper;
using Microsoft.Extensions.Options;

namespace e_services.Services
{
    public class ShopService : IShopRespository
    {
        private readonly ElasticClient _Shop;
        private readonly AppSettings _appSettings;
        private readonly string shopIndex = "shop";
        private readonly string hosterIndex = "hoster";

        public ShopService(IElasticSearchSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            var node = new Uri(settings.uri);

            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(shopIndex);

            _Shop = new ElasticClient(settingsNode);

        }

        public dynamic Create(Shop shop, string idHoster)
        {
            var existingShop = _Shop.Search<Shop>(s => s.Query(q => q.Match(m => m
            .Field(f => f.name).Query(shop.name))));

            
            if ((existingShop.MaxScore==0)||(!existingShop.Hits.FirstOrDefault().Source.name.Equals(shop.name)))
            {
                var myShop = _Shop.Index(shop, i => i.Index(shopIndex));
                shop.id = myShop.Id;
                if (myShop.IsValid)
                {
                    var indexResponse = _Shop.Get<Hshop>(idHoster, c => c.Index(hosterIndex));
                    if (indexResponse.IsValid)
                    {
                        indexResponse.Source.id = indexResponse.Id;
                        Hshop hoster_shops = indexResponse.Source;
                        if (hoster_shops.shop != null)
                        {
                            hoster_shops.shop.Add(myShop.Id);
                        }
                        else
                        {
                            hoster_shops.shop = new List<string>();
                            hoster_shops.shop.Add(myShop.Id);
                        }
                        _Shop.Update<Hshop>(idHoster, u => u.Doc(hoster_shops).Index(hosterIndex));

                        var RespShop = new
                        {
                            status = 200,
                            created = true,
                            description = "shop was regsitred successfully",
                            payload = shop
                        };

                        return RespShop;
                    }
                }
                var Rsp = new
                {
                    status = 400,
                    created = false,
                    description = "error when register the shop",
                    payload = new List<string>()
                };

                return Rsp;
            }
            else
            {
                var Rsp = new
                {
                    status = 400,
                    created = false,
                    description = "existing item name",
                    payload = existingShop.Hits.Select(h =>
                    {
                        return h.Source.name;
                    }).ToList()
                };
            
                return Rsp;
            }
            return null;

        }

        public List<Shop> getAll(int paginationSatrt, int paginationEnd)
        {
            var indexResponse = _Shop.Search<Shop>(s => s
           .From(paginationSatrt)
           .Size(paginationEnd)
           .Query(q => q.MatchAll()));
            if (indexResponse.IsValid)
            {
                return indexResponse.Hits.Select(h =>
                {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList();
            }
            return null;
        }

        public Shop getByIdShop(string id)
        {
            var indexResponse = _Shop.Get<Shop>(id);
            if (indexResponse.IsValid)
            {
                indexResponse.Source.id = indexResponse.Id;
                return indexResponse.Source;
            }
            return null;
        }

        public bool Remove(string idshop , string idHoster)
        {
            var hosterShop = _Shop.Get<Hshop>(idHoster,idx =>idx.Index(hosterIndex));
            if (hosterShop.Source != null)
            {
                Hshop newHshop = hosterShop.Source;
                if (newHshop.shop.Contains(idshop))
                {
                    newHshop.shop.Remove(idshop);
                    _Shop.Update<Hshop>(idHoster, idx => idx.Doc(newHshop).Index(hosterIndex));
                    var shopResponse = _Shop.Delete<Shop>(idshop, idx => idx.Index(shopIndex));
                    return shopResponse.IsValid;
                }

            }
            return false;
           
        }

        public bool Update(string idHoster ,string idShop, Shop shop)
        {
            var isHosterShop = _Shop.Get<Hshop>(idHoster, idx => idx.Index(hosterIndex));
            if(isHosterShop.Source!=null)
                if (isHosterShop.Source.shop.Contains(idShop))
                {
                    var isUpdated = _Shop.Update<Shop>(idShop, idx => idx.Doc(shop).Index(shopIndex));
                    return isUpdated.IsValid;
                }

            return false;
        }

        public Shop updatePhoto(string idShop, string filename)
        {
            var indexResponse = _Shop.Get<Shop>(idShop, idx => idx.Index(shopIndex));
            if (indexResponse.Source != null)
            {
                Shop newshop = indexResponse.Source;
                newshop.UrlPhoto = filename;
                var isUpdated = _Shop.Update<Shop>(idShop, idx =>idx.Doc(newshop).Index(shopIndex));
                if (isUpdated.IsValid)
                    return newshop;
                return null ;
            }
            return null;
        }

        public Shop updateVideo(string idShop, string filename)
        {
            var indexResponse = _Shop.Get<Shop>(idShop, idx => idx.Index(shopIndex));
            if (indexResponse.Source != null)
            {
                Shop newshop = indexResponse.Source;
                newshop.UrlVideo = filename;
                var isUpdated = _Shop.Update<Shop>(idShop, idx => idx.Doc(newshop).Index(shopIndex));
                if (isUpdated.IsValid)
                    return newshop;
                return null;
            }
            return null;
        }
    }
}
