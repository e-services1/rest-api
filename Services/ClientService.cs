﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models;
using Nest;
using e_services.Helper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using e_services.Helper;
using System.Text;
using e_services.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using e_services.Respository;

namespace e_services.Services
{
    
    public class ClientService : IUserRepository<Client>
    {
        private readonly ElasticClient _client;
        private readonly AppSettings _appSettings;
        private readonly string clientIndex = "client";
        private readonly string clientOuthIndex = "clientouth";


        public ClientService(IElasticSearchSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            var node = new Uri(settings.uri);
            
            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(clientIndex);  
           
            _client = new ElasticClient(settingsNode);

        }

        public List<Client> Get(int paginationSatrt, int paginationEnd)
        {
            var indexResponse = _client.Search<Client>(s => s
            .From(paginationSatrt)
            .Size(paginationEnd)
            .Query(q => q.MatchAll()));
            if (indexResponse.IsValid)
            {
                return indexResponse.Hits.Select(h =>
                {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList();
            }
            return null;
                

        }

        public Client Get(string id)
        {
            var indexResponse= _client.Get<Client>(id);
            if (indexResponse.IsValid)
            {
                indexResponse.Source.id = indexResponse.Id;
                return indexResponse.Source;
            }
            return null;
        }

        public Client Create(Client client, string password)
        {
            if (getUserByEmail(client.Email) != null)
                return null;

            ClientOuth clientouth = new ClientOuth();
            var indexResponse = _client.IndexDocument(client);

            HashPassword sha3 = new HashPassword();

            string passwordHash, privateKey;
            sha3.HashPasswordV3(password, out passwordHash, out privateKey);

            if (indexResponse.IsValid)
            {
                clientouth.passwordHash = passwordHash;
                clientouth.privateKey = privateKey;
                _client.Index(clientouth, i => i
                        .Index(clientOuthIndex)
                        .Id(indexResponse.Id)
                        );

                client.id = indexResponse.Id;
                return client;
            }

            return null;

        }

        public bool Update(string id, Client client)
        {
            var indexResponse = _client.Update<Client>(id, u => u.Doc(client));
            client.id = indexResponse.Id;
            return indexResponse.IsValid;
        }
   
        public bool Remove(string id) => _client.Delete<Client>(id).IsValid;

        public String Authenticate(string id, string password)
        {
            var client = _client.Get<ClientOuth>(id, idx => idx.Index(clientOuthIndex));
            if (client.IsValid)
            {
                HashPassword sha3 = new HashPassword();
                if (sha3.VerifyHashPasswordV3(password,client.Source.privateKey,client.Source.passwordHash))
                {
                    return TokenCreator.tokenGenerator(Role.Client, client.Id, _appSettings.Secret);          
                }
                else
                {
                    return null;
                }
            }
            return null;           
        }

        public Client getUserByEmail(string email)
        {
                        
            var client = _client.Search<Client>(s => s
            .Query(q => q
                .Match(m => m
                    .Field(f => f.Email)
                    .Query(email))
                        )
            );
           /* var client = _client.Search<Client>(s => s
                .Query(q => q
                    .DateRange(r => r
                        .Field(f => f.Email)
                        .Name(email)
                    )
                )
            );*/


            if ((client.IsValid) && (client.Hits.Count > 0))
            {
                Client c =  client.Hits.Select(h =>
                {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList().First();
                if (c.Email.Equals(email))
                    return c;
                return null;
            }
            return null;

        }

        public Client updatePhoto (string id , string filename)
        {
            Client c = Get(id);
            if (c != null)
            {
                c.UrlPhoto = filename;
                var indexResponse = _client.Update<Client>(id, u => u.Doc(c));
                if (indexResponse.IsValid)
                {
                    return c;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
          

        }

        public bool resetPassword(string email, string password)
        {
            Client client = getUserByEmail(email);

            if (client == null)
                return false;

            HashPassword sha3 = new HashPassword();
            string passwordHash, privateKey;
            sha3.HashPasswordV3(password, out passwordHash, out privateKey);
            ClientOuth outh = new ClientOuth();
            outh.passwordHash = passwordHash;
            outh.privateKey = privateKey;

            var indexResponse = _client.Update<ClientOuth>(client.id, u =>
            u.Index(clientOuthIndex)
            .Doc(outh));
            return indexResponse.IsValid;

        }

        public string signinorsignupWithFacebook(Client client)
        {
            if (getUserByEmail(client.Email) == null)
            {
                _client.IndexDocument(client);
            }
            return TokenCreator.tokenGenerator(Role.Client, client.id, _appSettings.Secret);


        }
    }
}
