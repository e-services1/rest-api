﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models.Hoster;
using Nest;
using e_services.Helper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using e_services.Helper;
using System.Text;
using e_services.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using e_services.Respository;
using e_services.Models;

namespace e_services.Services
{

    public class HosterServices : IUserRepository<Hoster>
    {
        private readonly ElasticClient _hoster;
        private readonly AppSettings _appSettings;
        private readonly string hosterIndex = "hoster";
        private readonly string hosterOuthIndex = "hosterouth";


        public HosterServices(IElasticSearchSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            var node = new Uri(settings.uri);

            var settingsNode = new ConnectionSettings(node)
                .BasicAuthentication(settings.username, settings.password)
                .DefaultIndex(hosterIndex);

            _hoster = new ElasticClient(settingsNode);

        }

        public List<Hoster> Get(int paginationSatrt, int paginationEnd)
        {
            var indexResponse = _hoster.Search<Hoster>(s => s
            .From(paginationSatrt)
            .Size(paginationEnd)
            .Query(q => q.MatchAll()));
            if (indexResponse.IsValid)
            {
                return indexResponse.Hits.Select(h =>
                {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList();
            }
            return null;


        }

        public Hoster Get(string id)
        {
            var indexResponse = _hoster.Get<Hoster>(id);
            if (indexResponse.IsValid)
            {
                indexResponse.Source.id = indexResponse.Id;
                return indexResponse.Source;
            }
            return null;
        }

        public Hoster Create(Hoster hoster, string password)
        {
            if (getUserByEmail(hoster.Email) != null)
                return null;

            HosterOuth hosterOuth = new HosterOuth();
            var indexResponse = _hoster.IndexDocument(hoster);

            HashPassword sha3 = new HashPassword();

            string passwordHash, privateKey;
            sha3.HashPasswordV3(password, out passwordHash, out privateKey);

            if (indexResponse.IsValid)
            {
                hosterOuth.passwordHash = passwordHash;
                hosterOuth.privateKey = privateKey;
                _hoster.Index(hosterOuth, i => i
                        .Index(hosterOuthIndex)
                        .Id(indexResponse.Id)
                        );

                hoster.id = indexResponse.Id;
                return hoster;
            }

            return null;

        }

        public bool Update(string id, Hoster hoster)
        {
            var indexResponse = _hoster.Update<Hoster>(id, u => u.Doc(hoster));
            hoster.id = indexResponse.Id;
            return indexResponse.IsValid;
        }

        public bool Remove(string id) => _hoster.Delete<Hoster>(id).IsValid;

        public String Authenticate(string id, string password)
        {
            var hoster = _hoster.Get<HosterOuth>(id, idx => idx.Index(hosterOuthIndex));
            if (hoster.IsValid)
            {
                HashPassword sha3 = new HashPassword();
                if (sha3.VerifyHashPasswordV3(password, hoster.Source.privateKey, hoster.Source.passwordHash))
                {
                    return TokenCreator.tokenGenerator(Role.Hoster, hoster.Id, _appSettings.Secret);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public Hoster getUserByEmail(string email)
        {

            var hoster = _hoster.Search<Hoster>(s => s
            .Query(q => q
                .Match(m => m
                    .Field(f => f.Email)
                    .Query(email))
                        )
            );
            /* var client = _client.Search<Client>(s => s
                 .Query(q => q
                     .DateRange(r => r
                         .Field(f => f.Email)
                         .Name(email)
                     )
                 )
             );*/


            if ((hoster.IsValid)&&(hoster.Hits.Count>0))
            {
                Hoster c = hoster.Hits.Select(h =>
                {
                    h.Source.id = h.Id;
                    return h.Source;
                }).ToList().First();
                if (c.Email.Equals(email))
                    return c;
                return null;
            }
            return null;

        }

        public Hoster updatePhoto(string id, string filename)
        {
            Hoster c = Get(id);
            if (c != null)
            {
                c.UrlPhoto = filename;
                var indexResponse = _hoster.Update<Hoster>(id, u => u.Doc(c));
                if (indexResponse.IsValid)
                {
                    return c;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }


        }

        public bool resetPassword(string email, string password)
        {
            throw new NotImplementedException();
        }

        public string signinorsignupWithFacebook(Hoster hoster)
        {
            throw new NotImplementedException();
        }
    }
}
