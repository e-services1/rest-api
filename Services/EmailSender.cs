﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
namespace e_services.Services
{

    public interface IEmailSender
    {
        Task<bool> SendEmailAsync(string email, string token);
    }

    public class AuthMessageSenderOptions
    {
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
    }

    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        public Task<bool> SendEmailAsync(string email ,string token)
        {
            return Execute(Options.SendGridKey, email ,token);
        }

        public Task<bool> Execute(string apiKey, string email ,string token)
        {
            EmailBody myemail = new EmailBody();
            myemail.email = email;
            myemail.token = token;

            var sendGridClient = new SendGridClient(apiKey);

            var sendGridMessage = new SendGridMessage();
            sendGridMessage.SetFrom("khomsa@gmail.com", "Khomsa services");
            sendGridMessage.AddTo(new EmailAddress(email));
            sendGridMessage.SetTemplateId("d-78b38ef7c0e74bb480f8196ea9954e1c");
            sendGridMessage.SetTemplateData(myemail);
            //sendGridMessage.SetClickTracking(false, false);
            var response=  sendGridClient.SendEmailAsync(sendGridMessage);
            if (response.Result.StatusCode == System.Net.HttpStatusCode.Accepted)
                return Task.FromResult(true);
            return Task.FromResult(false);
        }

        private class EmailBody
        {
            [JsonProperty("email")]
            public string email { get; set; }

            [JsonProperty("token")]
            public string token { get; set; }

        }
    }

}
