﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using e_services.Models;
namespace e_services.Utility
{
    public class Request
    {
        public class modelFacebookisLoggeed
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class modelFacebookUserValue
        {
            public string id { get; set; }
            public string email { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public Picture picture { get; set;  }

            public class Picture
            {public Data data { get; set; }
                
                public class Data
                {
                    public string url { get; set; }
                }
            }


        }

        static HttpClient client = new HttpClient();
        
        public static async Task<bool> facebookVerifyTokenIsLogged(string token,string id)
        {
            string path = "https://graph.facebook.com/v6.0/";
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(path+id);
            if (response.IsSuccessStatusCode)
            {
                modelFacebookisLoggeed data = JsonConvert.DeserializeObject<modelFacebookisLoggeed>(await response.Content.ReadAsStringAsync());
                if (data.id.Equals(id))
                {
                    return true;
                }
            }
            return false;

        }

        public static async Task<Client> facebookUserValue(string token, string id)
        {
            string path = "https://graph.facebook.com/v6.0/"+id+"?fields=picture,email,id,first_name,last_name";
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                modelFacebookUserValue data = JsonConvert.DeserializeObject<modelFacebookUserValue>(await response.Content.ReadAsStringAsync());
                if (data !=null)
                {
                    Client c = new Client();
                    c.FirstName = data.first_name;
                    c.LastName = data.last_name;
                    c.Email = data.email;
                    c.UrlPhoto = data.picture.data.url;
                    c.id = "facebook"+data.id;
                    return c;
                }
            }
            return null;

        }
    }
}
