﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Utility
{
    public static class Logger
    {
       public static void log (string s)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine(s);
        }
    }
}
