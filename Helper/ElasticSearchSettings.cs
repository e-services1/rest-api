﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models
{
    public class ElasticSearchSettings : IElasticSearchSettings
    {
        public string username {
            get ;
            set ;
        }
        public string password {
            get ; 
            set ;
        }
        public string uri { 
            get ;
            set ;
        }
    }
}
