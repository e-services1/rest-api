﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models
{
    public interface IElasticSearchSettings
    {
        string username { get; set; }
        string password { get; set; }
        string uri { get; set; }
    }
}
