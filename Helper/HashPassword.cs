﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;

namespace e_services.Helper
{
    public class HashPassword : IHashpassword
    {

        public void HashPasswordV3(string pswd, out string outHash, out string outSalt)
        {

            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            outSalt = Convert.ToBase64String(salt);


            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed1 = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: pswd,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            string hashed2 = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: pswd,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            outHash = hashed1;



        }

        public Boolean VerifyHashPasswordV3(string pswd, string salt, string hashedPassword)
        {

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: pswd,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            if (hashedPassword.Equals(hashed))
                return true;
            return false;

        }

    }
}
