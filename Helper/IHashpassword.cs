﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Helper
{
    public interface IHashpassword
    {
        void HashPasswordV3(string pswd, out string hash, out string key);
        Boolean VerifyHashPasswordV3(string pswd , string key , string hashed);
    }
}
