﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Helper
{
    public static class FileHelper
    {
        public static IWebHostEnvironment _environment;

        public static string FileUploader (string foldername, FileUploader files, IWebHostEnvironment environment)
        {
            _environment = environment;
            string folderName = foldername;
            string unixTimestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();


            if (files.files.Length > 0)
            {
                string fileName = unixTimestamp + files.files.FileName;
                try
                {
                    if (!Directory.Exists(_environment.WebRootPath + folderName))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + folderName);
                    }
                    using (FileStream filestream =
                        File.Create(_environment.WebRootPath + folderName + fileName))
                    {

                        files.files.CopyTo(filestream);
                        filestream.Flush();
                        return fileName;

                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
