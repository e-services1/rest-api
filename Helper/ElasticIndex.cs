﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Helper
{
    public static class ElasticIndex
    {
        public static string followProductIndex = "followproduct";
        public static string followShopIndex = "followshop";
        public static string orderIndex = "order";
        public static string clientOrderIndex = "clientorder";
        public static string stayIndex = "pstay";
        public static string adventureIndex = "padventure";
        public static string productIndex = "pproduct";
    }
}

