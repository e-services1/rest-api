﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models
{
    public class FollowedShop
    {
        public List<string> shop { get; set; }
    }
}
