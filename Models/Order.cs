﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models.Product;
namespace e_services.Models
{
    public class Order
    {
        public string id { get; set; }
        public string idClient { get; set; }
        public string idProduct { get; set; }
        public Category category { get; set; }
        public long timestamp { get; set; }
        public bool verifyed { get; set; }

    }
}
