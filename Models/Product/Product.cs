﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models.Product
{
    /// <summary>
    ///  score or rate attribute must be in other index, 
    ///  caused by it required many update
    /// </summary>
    public class Product
    {
        public string id { get; set; }
        public string idShop { get; set; }
        public string idHoster { get; set; }
        
        [Required]
        public Category category { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "title cannot be longer than 25 characters.")]
        public string title { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 5, ErrorMessage = "description cannot be longer than 150 characters.")]
        public string description { get; set; }

        [Required]
        public double cost { get; set; }

        public int like { get; set; }

        public List<string> urlPhotos { get; set; }


    }
}
