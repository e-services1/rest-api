﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models.Product
{
    public class Location
    {
        [Required]
        public double lat { get; set; }

        [Required]
        public double lon { get; set; }

    }
}
