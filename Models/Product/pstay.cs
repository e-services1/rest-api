﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models.Product
{
    public class pStay : Product
    {
        /// <summary>
        /// guest is refer to the number of guests
        /// </summary>
        [Required]
        public int guest { get; set; }

        public Location location { get; set; }

        public string adresse { get; set; }

        /// <summary>
        /// dateDeb and dateFin is unix timestamp long
        /// </summary>
        public long dateDeb { get; set; }

        public long dateFin { get; set; }


    }
}
