﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models.Product
{
    public class pProduct :Product
    {  
        /// <summary>
        /// material is refer to material of product
        /// like seramic or lane or ...
        /// </summary>
        public string material { get; set; }
    }
}
