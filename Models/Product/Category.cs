﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace e_services.Models.Product
{
    /// <summary>
    /// stay = 2
    /// adventure = 3
    /// product = 4
    /// </summary>
    public enum Category
    {
        stay=2,
        adventure=3,
        product=4
    }
}
