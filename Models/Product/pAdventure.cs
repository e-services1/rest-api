﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models.Product
{
    public class pAdventure : Product
    {        
        public Location location { get; set; }

        public string adresse { get; set; }

        public long dateDeb { get; set; }

        public long dateFin { get; set; }
    }
}
