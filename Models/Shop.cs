﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace e_services.Models
{
    public class Shop
    {
        [JsonPropertyName("_IdShop")]
        public string id { get; set; }

        [JsonPropertyName("_IdHoster")]
        public string idHoster { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "shop name cannot be longer than 10 characters.")]
        public string name { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "shop description cannot be longer than 20 characters.")]
        public string description { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "verify your address")]
        public string address { get; set; }

        public string UrlPhoto { get; set; }
        public string UrlVideo { get; set; }

        public double lat { get; set; }
        public double lan { get; set; }

        public List<string> pStay { get; set; }
        public List<string> pAdventure { get; set; }
        public List<string> pProduct { get; set; }


    }
}
