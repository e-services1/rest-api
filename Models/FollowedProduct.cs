﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Models
{
    public class FollowedProduct
    {
        public List<string> stay { get; set; }
        public List<string> adventure { get; set; }
        public List<string> product { get; set; }

    }
}
