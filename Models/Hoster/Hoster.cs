﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace e_services.Models.Hoster
{
    public class Hoster
    {
        [JsonPropertyName("_Id")]
        public string id { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 2, ErrorMessage = "your first name cannot be longer than 10 characters.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "your last name cannot be longer than 20 characters.")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "verify your email address")]
        public string Email { get; set; }

        public string UrlPhoto { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "your phone cannot be longer than 20 characters.")]
        public string Phone { get; set; }
    }
}
