﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace e_services.Models
{
    public class ClientOuth
    {
        
        [Required]
        [DataType(DataType.Password)]
        public string passwordHash { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string privateKey { get; set; }

    }
}
