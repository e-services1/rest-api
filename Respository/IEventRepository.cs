﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models;

namespace e_services.Respository
{
    public interface IEventRepository
    {
        List<Event> getAllEvent(int paginationStart , int paginationEnd);
    }
}
