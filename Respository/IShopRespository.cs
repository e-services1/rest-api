﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models;

namespace e_services.Respository
{
    public interface IShopRespository
    {
        List<Shop> getAll(int paginationSatrt, int paginationEnd);
        Shop getByIdShop(string idShop);
        dynamic Create(Shop shop, string idHoster);
        bool Update(string idHoster ,string idShop, Shop shop);
        bool Remove(string idShop,string idHoster);
        Shop updatePhoto(string idShop, string filename);
        Shop updateVideo(string idShop, string filename);

    }
}
