﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_services.Respository
{
    /// <summary>
    /// Generic Repository for Client and Hoster, that contain all 5 crud function (4+ get all), plus outh
    /// + post picture
    /// </summary>
    public interface IUserRepository<Iuser>
    {
         List<Iuser> Get(int paginationSatrt, int paginationEnd);
         Iuser Get(string id);
         Iuser Create(Iuser actor, string password);
         bool Update(string id, Iuser actor);
         bool Remove(string id);
         String Authenticate(string id, string password);
         Iuser updatePhoto(string id, string filename);
         Iuser getUserByEmail(string email);
         bool resetPassword(string email, string password);
         String signinorsignupWithFacebook(Iuser user);
    }
}
