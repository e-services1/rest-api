﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models.Product;
namespace e_services.Respository
{
    public interface IProductRepository
    {
        dynamic Create (dynamic product, string idHoster, string idShop);
        dynamic Get(string idPrduct, Category c);
        dynamic get(Category c, int from , int size);
        bool Update(Product prod);
        bool Remove(Product prod);
        dynamic updatePhoto(string id, string filename,Category c);
        List<Product> searchByName(string name ,int from , int size);
        List<Product> searchByCategory(Category c ,string name, int from , int size);

    }
}
