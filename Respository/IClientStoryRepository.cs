﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_services.Models;
using e_services.Models.Product;

namespace e_services.Respository
{
    public interface IClientStoryRepository
    {
        //---- follow product
        FollowedProduct getFollowedProducts(string iduser);
        FollowedProduct PostFollowedProducts(string iduser,Category c ,string idproduct);
        FollowedProduct deleteFollowedProducts(string iduser,Category c,string idproduct);
        //---- follow shop
        FollowedShop getFollowedShop(string iduser);
        FollowedShop postFollowedShop(string iduser,string shop);
        FollowedShop deleteFollowedShop(string iduser, string idshop);
        //---- order ----
        Clientorder postOrder(string id, Order order );
        Clientorder getOrder(string id);
        Order getOrderById(string idclient, string idOrder);
        Clientorder deleteOrder(string id,string idorder);
    }
}
